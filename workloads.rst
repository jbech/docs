**************************
Tests Suites and Workloads
**************************

This document explains how build individual tests and
run on Morello Fixed Virtual Platform (Morello FVP)

Android tests suites and workloads - build and run
==================================================

Prerequisites:

On the host machine, after having built Android following the instructions
in the "Android Stack" section of the `user guide`_, and started FVP as
per the "Running the software on FVP" section:

::

        cd <morello_workspace>/android/
        source build/envsetup.sh
        export LLVM_PREBUILTS_VERSION="clang-local"
        export LLVM_RELEASE_VERSION="$(cd prebuilts/clang/host/linux-x86/clang-local/lib64/clang; echo *)"
        lunch morello_fvp_nano-eng
        m <test name>
        adb push <source path> <destination path> //please refer to individual test case build and run steps

All tests should be run as root and may fail otherwise. There are two possible options:

- On the console, ``su`` must be run to become root (the default login user is ``shell``).
- When using ``adb shell``, the login user is already root and no additional step is required.

Bionic unit tests
-----------------

Steps to build and run individual tests:

Build:

.. code-block:: sh

 m bionic-unit-tests-static
 adb push out/target/product/morello/data/nativetest64/bionic-unit-tests-static/bionic-unit-tests-static  \
 /data/nativetest64/bionic-unit-tests-static/bionic-unit-tests-static
 adb push out/target/product/morello/data/nativetestc64/bionic-unit-tests-static/bionic-unit-tests-static  \
 /data/nativetestc64/bionic-unit-tests-static/bionic-unit-tests-static

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

 /data/nativetest64/bionic-unit-tests-static/bionic-unit-tests-static
 /data/nativetestc64/bionic-unit-tests-static/bionic-unit-tests-static

servicemanager
--------------

Steps to build and run individual tests:

binderDriverInterfaceTest
^^^^^^^^^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

   m binderDriverInterfaceTest
   adb push out/target/product/morello/data/nativetest64/binderDriverInterfaceTest/binderDriverInterfaceTest \
   /data/nativetest64/binderDriverInterfaceTest/binderDriverInterfaceTest

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    /data/nativetest64/binderDriverInterfaceTest/binderDriverInterfaceTest

binderLibTest
^^^^^^^^^^^^^

Build

.. code-block:: sh

    m binderLibTest
    adb push out/target/product/morello/data/nativetest64/binderLibTest/binderLibTest \
    /data/nativetest64/binderLibTest/binderLibTest

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    /data/nativetest64/binderLibTest/binderLibTest


binderSafeInterfaceTest
^^^^^^^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

    m binderSafeInterfaceTest
    adb push out/target/product/morello/data/nativetest64/binderSafeInterfaceTest/binderSafeInterfaceTest \
    /data/nativetest64/binderSafeInterfaceTest/binderSafeInterfaceTest

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

     ( ulimit -n 128 && /data/nativetest64/binderSafeInterfaceTest/binderSafeInterfaceTest )


binderTextOutputTest
^^^^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

    m binderTextOutputTest
    adb push out/target/product/morello/data/nativetest64/binderTextOutputTest/binderTextOutputTest \
    /data/nativetest64/binderTextOutputTest/binderTextOutputTest

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

    /data/nativetest64/binderTextOutputTest/binderTextOutputTest



binderThroughputTest
^^^^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

    m binderThroughputTest
    adb push out/target/product/morello/data/nativetest64/binderThroughputTest/binderThroughputTest \
    /data/nativetest64/binderThroughputTest/binderThroughputTest

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

     /data/nativetest64/binderThroughputTest/binderThroughputTest -i 1000 -p


binderValueTypeTest
^^^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

    m binderValueTypeTest
    adb push out/target/product/morello/data/nativetest64/binderValueTypeTest/binderValueTypeTest \
    /data/nativetest64/binderValueTypeTest/binderValueTypeTest

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

      /data/nativetest64/binderValueTypeTest/binderValueTypeTest

logd service
------------
Steps to build and run individual tests:

liblog-unit-tests
^^^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

   m liblog-unit-tests
   adb push out/target/product/morello/data/nativetest64/liblog-unit-tests/liblog-unit-tests \
   /data/nativetest64/liblog-unit-tests/liblog-unit-tests
   adb push out/target/product/morello/data/nativetestc64/liblog-unit-tests/liblog-unit-tests \
   /data/nativetestc64/liblog-unit-tests/liblog-unit-tests

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

  /data/nativetest64/liblog-unit-tests/liblog-unit-tests
  /data/nativetestc64/liblog-unit-tests/liblog-unit-tests


logd-unit-tests
^^^^^^^^^^^^^^^

Build:

.. code-block:: sh

  m logd-unit-tests
  adb push out/target/product/morello/data/nativetest64/logd-unit-tests/logd-unit-tests \
  /data/nativetest64/logd-unit-tests/logd-unit-tests
  adb push out/target/product/morello/data/nativetestc64/logd-unit-tests/logd-unit-tests \
  /data/nativetestc64/logd-unit-tests/logd-unit-tests

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

  /data/nativetest64/logd-unit-tests/logd-unit-tests
  /data/nativetestc64/logd-unit-tests/logd-unit-tests


liblog-benchmarks
^^^^^^^^^^^^^^^^^

Note: This is not an actual benchmark when run on FVP

Build:

.. code-block:: sh

  m liblog-benchmarks
  adb push out/target/product/morello/data/benchmarktestc64/liblog-benchmarks/liblog-benchmarks \
  /data/benchmarktestc64/liblog-benchmarks/liblog-benchmarks
  adb push out/target/product/morello/data/benchmarktest64/liblog-benchmarks/liblog-benchmarks \
  /data/benchmarktest64/liblog-benchmarks/liblog-benchmarks

Run: on FVP run the workloads using the commands lines below

.. code-block:: sh

  /data/benchmarktest64/liblog-benchmarks/liblog-benchmarks  \
    --benchmark_repetitions=1 --benchmark_min_time=0.001
  /data/benchmarktestc64/liblog-benchmarks/liblog-benchmarks \
    --benchmark_repetitions=1 --benchmark_min_time=0.001

Basic demo of capability-based compartmentalization
---------------------------------------------------

Steps to build and run test

.. code-block:: sh

 m compartment-demo
 adb push out/target/product/morello/data/nativetest64/compartment-demo/ \
 /data/nativetest64/compartment-demo/

on FVP run the workloads using the commands lines below

.. code-block:: sh

 /data/nativetest64/compartment-demo/compartment-demo

.. _user guide:
 user-guide.rst
